/**
 * \file
 * \brief Main code block.
 *
 * \author Copyright (C) 2014-2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "distortos/board/leds.hpp"
#include "distortos/distortosConfiguration.h"
#include "distortos/board/usarts.hpp"
#include "distortos/ThisThread.hpp"
#include "distortos/StaticThread.hpp"
#include "distortos/SerialPort/SerialPort.hpp"

#include "distortos/Mutex.hpp"
#include "distortos/ConditionVariable.hpp"

#include <string.h>
#include "distortos/string/dma_string.hpp"


volatile unsigned int timeus = 0;

distortos::Mutex m;
distortos::ConditionVariable cv;
bool flag;


void receiveHandler(distortos::SerialPort& serial) __attribute__((noreturn));
void blink();

void testPrintNumbers(distortos::SerialPort& serial);
void testWriteReadSerialClass(distortos::SerialPort& serial);
void testWriteReadUsartBaseClass(distortos::chip::usart::ChipUsart& usart);



int main()
{
	auto& usart6 = distortos::board::usart6;
	distortos::SerialPort sp { usart6 };
	//sp.startAsyncRead(512, 4, receiveHandler);

	unsigned int i{};
	while(1)
	{

		testPrintNumbers(sp);
		testWriteReadSerialClass(sp);
		testWriteReadUsartBaseClass(sp.getUsartObject());

		sp << "Hello World: " << i++ << distortos::endl << distortos::flush;
		distortos::ThisThread::sleepFor(std::chrono::milliseconds{ 1000 });
	}
}



void testPrintNumbers(distortos::SerialPort& serial)
{
	const int i { 1234 };
	const long l { 31413431 };
	const long long ll { -74030497310 };
	const unsigned u { 3434 };
	const unsigned long ul { 43434343 };
	const unsigned long long ull { 698796986 };
	const float f { 4354.5454 };
	const double d { -34.5454543497697986986 };
	const long double ld { 535425.542542587687686 };
	const char *chp { "print literal (char pointer)" };

	/* Numeric data */
	serial
	<< "   Numeric "   				<< distortos::endl << distortos::flush
	<< "int         : " << i 		<< distortos::endl << distortos::flush
	<< "long        : " << l  		<< distortos::endl << distortos::flush
	<< "long long   : " << ll  		<< distortos::endl << distortos::flush
	<< "unsigned    : " << u  		<< distortos::endl << distortos::flush
	<< "u long      : " << ul  		<< distortos::endl << distortos::flush
	<< "u long long : " << ull  	<< distortos::endl << distortos::flush
	<< "float       : " << f  		<< distortos::endl << distortos::flush;

	serial.setPrecision(8);

	serial
	<< "double      : " << d  		<< distortos::endl << distortos::flush
	<< "long double : " << ld  		<< distortos::endl << distortos::flush
	<< "hex value   : " << distortos::hex(0x12345678) << distortos::endl << distortos::flush
	<< distortos::endl << distortos::flush;

	/* Characters data */
	serial
	<< "   Characters " 			<< distortos::endl << distortos::flush
	<< "const char* : " <<  chp 	<< distortos::endl << distortos::flush
	<< "*char_ptr   : " << *chp 	<< distortos::endl << distortos::flush
	<< "char_ptr[n] : " <<  chp[3] 	<< distortos::endl << distortos::flush
	<< distortos::endl << distortos::flush;
}


void testWriteReadSerialClass(distortos::SerialPort& serial)
{
	/* read write (string)*/
	serial
	<< "Test1: readLine(SerialPort)... " <<  distortos::endl << distortos::flush
	<< "> ";
	std::string line { serial.readLine() };
	serial << distortos::endl << distortos::flush;
	serial << "Test1 done : " << line  << distortos::flush;



	/* write line */
	serial.writeLn("Test2: Write line function");



	/* write array */
	char buf[] { "Test3: Print regular array data\n\r" };
	serial.write(buf);



	/* write std::array */
	std::array<char, sizeof( "Test4: Print std::array data\n\r" )> array = { { "Test4: Print std::array data\n\r"} };
	serial.write(array);



	/* write known size buffer */
	const char *helloPrint { "Test5 write bufffer(ptr,size)\n\r" };
	serial.write(helloPrint, strlen(helloPrint));



	/* Print end test */
	serial << "SerialPort Test: End" << distortos::endl << distortos::flush
		   << distortos::endl << distortos::flush;
}



void testWriteReadUsartBaseClass(distortos::chip::usart::ChipUsart& usart)
{
	const std::size_t bufSize { 10 };
	char buf[bufSize];
	const char *helloPrint { "Test: readLine (usart)... \n\r> " };
	const char  *donePrint { "Test done: " };
	const char   *endPrint { "End" };

	usart.write(helloPrint, strlen(helloPrint));

	usart.read(buf, bufSize);

	usart.write(donePrint, strlen(donePrint));
	usart.write("\n\r",2);

	usart.write(buf, bufSize);
	usart.write("\n\r",2);

	usart.write(endPrint, strlen(endPrint));
	usart.write("\n\r",2);
}


void receiveHandler(distortos::SerialPort& serial)
{
	auto &led = distortos::board::leds[0];

	std::string readln {};

	while(1)
	{
		readln = serial.readLine();

		readln.erase(std::remove( readln.begin(), readln.end(), distortos::endl ), readln.end() );
		readln.erase(std::remove( readln.begin(), readln.end(), distortos::flush), readln.end() );

		if (readln == "led on")
		{
			led.set(1);
			serial << "led on" << distortos::endl << distortos::flush;
		}

		else if (readln == "led off")
		{
			led.set(0);
			serial << "led off" << distortos::endl << distortos::flush;
		}

		else if (readln == "led status")
		{
			if(led.get())
				serial << "led on" << distortos::endl << distortos::flush;
			else
				serial << "led off" << distortos::endl << distortos::flush;
		}
	}
}


void blink()
{
	auto &led = distortos::board::leds[0];
	while(1)
	{
		led.set(!led.get());
		distortos::ThisThread::sleepFor(std::chrono::milliseconds{ 500 });
	}
}
