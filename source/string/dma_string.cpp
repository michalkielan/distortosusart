/*
 * dma_string.hpp
 *
 *  Created on: Apr 21, 2016
 *      Author: michal
 */


#include "distortos/string/dma_string.hpp"



void* dma_memcpy(void* _dest, const void* _src, std::size_t _count)
{
	volatile auto& dma = *DMA2;
	volatile auto& dma_stream = *DMA2_Stream0;

	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

	dma_stream.CR = 0;
	__DSB();

	dma_stream.PAR = reinterpret_cast<uint32_t>(_src);		// from address
	dma_stream.M0AR = reinterpret_cast<uint32_t>(_dest);		// to address
	dma_stream.NDTR = _count;								// number of data to transfer
	dma_stream.CR |= DMA_SxCR_MINC;							// memory address ptr is incremented after each data transfer
	dma_stream.CR |= DMA_SxCR_PINC;							// perpih address ptr is incremented after each data transfer
	dma_stream.CR |= DMA_SxCR_DIR_1;						// direction - memory to memory
	dma_stream.CR |= DMA_SxCR_EN;							// stream enable

	__DSB();

	while ((dma.LISR & DMA_LISR_TCIF0) == 0);				// wait for transmit data done

	dma_stream.CR &= ~DMA_SxCR_EN;							// disable channel
	dma.LIFCR |= DMA_LIFCR_CTCIF0;							// clear stream 0 transmit data done flag

	return _dest;
}


char* dma_strcpy(char* _dest, const char* _src)
{
	return static_cast<char *>(dma_memcpy( static_cast<void *>(_dest), static_cast<const void *>(_src), strlen(_src)) );
}


char* dma_strncpy(char* _dest, const char* _src,  std::size_t _num)
{
	return static_cast<char *>(dma_memcpy(static_cast<void *>(_dest), static_cast<const void *>(_src), _num) );
}


char* dma_strcat(char* _dest, const char* _src)
{
	return static_cast<char *>(dma_memcpy( static_cast<void *>(_dest + strlen(_dest) ) , static_cast<const void *>(_src), strlen(_src)) );
}

