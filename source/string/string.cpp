
#include "distortos/string/string.hpp"


namespace distortos
{

unsigned int floatingPointPrecision {2};


std::string to_string( int value )
{
	return signed_to_string(value);
}


std::string to_string( long value )
{
	return signed_to_string(value);
}


std::string to_string( long long value )
{
	return signed_to_string(value);
}


std::string to_string( unsigned value )
{
	return unsigned_to_string(value);
}


std::string to_string( unsigned long value )
{
	return unsigned_to_string(value);
}


std::string to_string( unsigned long long value )
{
	return unsigned_to_string(value);
}


std::string to_string( float value)
{
	return fp_to_string(value, floatingPointPrecision);
}


std::string to_string( double value)
{
	return fp_to_string(value, floatingPointPrecision);
}


std::string to_string( long double value)
{
	return fp_to_string(value, floatingPointPrecision);
}


}	// namespace distortos
