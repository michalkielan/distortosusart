/*
 * serial.h
 *
 *  Created on: Apr 8, 2016
 *      Author: michal
 */

#include "distortos/SerialPort/SerialPort.hpp"

namespace distortos
{


SerialPort::SerialPort(chip::usart::ChipUsart& usart_) : usart{usart_}
{
}


SerialPort::SerialPort(chip::usart::ChipUsart& usart_,
		const uint32_t baudrate) : usart(usart_)
{
	usart.configureSetBaudrate(baudrate);
}


SerialPort::SerialPort(chip::usart::ChipUsart& usart_,
		const uint32_t baudrate,
		distortos::chip::Parity parity) : usart(usart_)
{
	usart.configureSetBaudrate(baudrate);
	usart.configureSetParity(parity);
}


SerialPort::SerialPort(chip::usart::ChipUsart& usart_,
		const uint32_t baudrate,
		distortos::chip::Parity parity,
		distortos::chip::WordLength wordLength) : usart(usart_)
{
	usart.configureSetBaudrate(baudrate);
	usart.configureSetParity(parity);
	usart.configureSetWordLength(wordLength);

}


SerialPort::SerialPort(chip::usart::ChipUsart& usart_,
		const uint32_t baudrate,
		distortos::chip::Parity parity,
		distortos::chip::WordLength wordLength,
		distortos::chip::StopBits stopBits) : usart(usart_)
{
	usart.configureSetBaudrate(baudrate);
	usart.configureSetParity(parity);
	usart.configureSetWordLength(wordLength);
	usart.configureSetStopBits(stopBits);
}


chip::usart::ChipUsart& SerialPort::getUsartObject()
{
	return usart;
}


void SerialPort::setPrecision(decltype(floatingPointPrecision) setPrecision)
{
	floatingPointPrecision = setPrecision;
}


void SerialPort::writeProtected(chip::usart::ChipUsart& u, const char* data, std::size_t len)
{
	writeLocker.lock();
	u.write(data, len);
	writeLocker.unlock();
}


void SerialPort::write(const std::string& data)
{
	writeProtected(usart, data.c_str(), data.length());
}


void SerialPort::writeLn(const std::string& data)
{
	write(data);
	usart.write(endl);
	usart.write(flush);
}


void SerialPort::write(const char* buf, std::size_t N)
{
	writeProtected(usart, buf, N);
}


std::string SerialPort::readTo(const char end)
{
	std::string line {};

	for(;;)
	{
		auto tmp = usart.read();
		line += tmp;

		if(tmp == end)
			break;
	}

	return line;
}


std::string SerialPort::readLine()
{
	return readTo('\n');
}


SerialPort& SerialPort::operator<<(const std::string& data)
{
	write(data);
	return *this;
}


SerialPort& SerialPort::operator<<(char byte)
{
	writeLocker.lock();
	usart.write(byte);
	writeLocker.unlock();
	return *this;
}


SerialPort& SerialPort::operator<<(const char* data)
{
	write(data, strlen(data));
	return *this;
}


}	// namespace distortos
