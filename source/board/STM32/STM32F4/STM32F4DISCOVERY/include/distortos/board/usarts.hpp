#ifndef SOURCE_BOARD_STM32_STM32F4_STM32F4DISCOVERY_INCLUDE_DISTORTOS_BOARD_USARTS_HPP_
#define SOURCE_BOARD_STM32_STM32F4_STM32F4DISCOVERY_INCLUDE_DISTORTOS_BOARD_USARTS_HPP_

#include "distortos/chip/ChipUsartPolling.hpp"
#include "distortos/chip/ChipUsartInterrupt.hpp"
#include "distortos/chip/ChipUsartDMA.hpp"

//#define CONFIG_BOARD_USART6_INTERRUPT_ENABLE
//#define CONFIG_BOARD_USART6_POLLING_ENABLE
#define CONFIG_BOARD_USART6_DMA_ENABLE

namespace distortos
{


namespace chip
{


}	// namespace chip


namespace board
{

#ifdef CONFIG_BOARD_USART6_INTERRUPT_ENABLE
extern chip::usart::ChipUsartInterrupt usart6;
#endif	// CONFIG_BOARD_USART6_INTERRUPT_ENABLE

#ifdef CONFIG_BOARD_USART6_POLLING_ENABLE
extern chip::usart::ChipUsartPolling usart6;
#endif	// CONFIG_BOARD_USART6_POLLING_ENABLE

#ifdef CONFIG_BOARD_USART6_DMA_ENABLE
extern chip::usart::ChipUsartDMA usart6;
#endif	// CONFIG_BOARD_USART6_DMA_ENABLE


}	// namespace board

}	// namespace distortos

#endif	// SOURCE_BOARD_STM32_STM32F4_STM32F4DISCOVERY_INCLUDE_DISTORTOS_BOARD_USARTS_HPP_
