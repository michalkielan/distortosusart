/**
 * \file
 * \brief Definition of LEDs for STM32F4DISCOVERY
 *
 * \author Copyright (C) 2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "distortos/board/usarts.hpp"
#include "distortos/chip/STM32F4-DMA.hpp"

namespace distortos
{

namespace board
{

/*---------------------------------------------------------------------------------------------------------------------+
| global usart objects
+---------------------------------------------------------------------------------------------------------------------*/


/**
 * \Usart6 DMA Mode
 */
#ifdef CONFIG_BOARD_USART6_DMA_ENABLE
chip::usart::ChipUsartDMA usart6
{
	chip::Pin::pc6, 						// tx pin
	chip::Pin::pc7, 						// rx pin
	chip::PinAlternateFunction::af8, 		// tx af
	chip::PinAlternateFunction::af8, 		// rx af
	USART6,									// usart type
	chip::dma::DmaStream::usart6_tx_stream6, // dma stream type
	DMA2_Stream6_IRQn,						// dma IRQn configure
	chip::PeriphClockSpeed::APB2			// usart clock
};

extern "C" void DMA2_Stream6_IRQHandler( void )
{
	usart6.DMA_IRQHandler();
}
#endif	// CONFIG_BOARD_USART6_DMA_ENABLE




/**
 * \Usart6 Interrupt Mode
 */
#ifdef CONFIG_BOARD_USART6_INTERRUPT_ENABLE
chip::usart::ChipUsartInterrupt usart6
{
	chip::Pin::pc6, 						// tx pin
	chip::Pin::pc7, 						// rx pin
	chip::PinAlternateFunction::af8, 		// tx af
	chip::PinAlternateFunction::af8, 		// rx af
	USART6,
	USART6_IRQn,
	chip::PeriphClockSpeed::APB2
};

extern "C" void USART6_IRQHandler()
{
	usart6.IRQHandler();
}
#endif	// CONFIG_BOARD_USART6_INTERRUPT_ENABLE




/**
 * \Usart6 Polling Mode
 */
#ifdef CONFIG_BOARD_USART6_POLLING_ENABLE
chip::usart::ChipUsartPolling usart6
{
	chip::Pin::pc6, 						// tx pin
	chip::Pin::pc7, 						// rx pin
	chip::PinAlternateFunction::af8, 		// tx af
	chip::PinAlternateFunction::af8, 		// rx af
	USART6,
	chip::PeriphClockSpeed::APB2
};
#endif	// CONFIG_BOARD_USART6_POLLING_ENABLE



}	// namespace board

}	// namespace distortos




