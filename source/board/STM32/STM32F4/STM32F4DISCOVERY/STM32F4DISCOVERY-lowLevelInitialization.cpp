/**
 * \file
 * \brief board::lowLevelInitialization() implementation for STM32F4DISCOVERY
 *
 * \author Copyright (C) 2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "distortos/board/lowLevelInitialization.hpp"

#include "distortos/chip/CMSIS-proxy.h"

namespace distortos
{

namespace board
{

/*---------------------------------------------------------------------------------------------------------------------+
| global functions
+---------------------------------------------------------------------------------------------------------------------*/
static inline void lowLevelUsartInitialization()
{
	/// GPIO clock enable
	RCC->AHB1ENR |=
			RCC_AHB1ENR_GPIOAEN |
			RCC_AHB1ENR_GPIOBEN |
			RCC_AHB1ENR_GPIOCEN |
			RCC_AHB1ENR_GPIODEN |
			RCC_AHB1ENR_GPIOEEN |
			RCC_AHB1ENR_GPIOFEN |
			0;

	/// USART clock enable
	RCC->APB1ENR |=
			RCC_APB1ENR_USART2EN |
			RCC_APB1ENR_USART3EN |
			RCC_APB1ENR_UART4EN  |
			RCC_APB1ENR_UART5EN  |
			0;

	RCC->APB2ENR |=
			RCC_APB2ENR_USART1EN |
			RCC_APB2ENR_USART6EN |
			0;
}


static inline void lowLevelLedInitialization()
{
	RCC->AHB1ENR |=
			RCC_AHB1ENR_GPIODEN |
			0;
}

static inline void lowLewelButtonsInitialization()
{
	RCC->AHB1ENR |=
			RCC_AHB1ENR_GPIOAEN |
			0;
}



void lowLevelInitialization()
{

#ifdef CONFIG_BOARD_BUTTONS_ENABLE
	lowLewelButtonsInitialization();
#endif	// def CONFIG_BOARD_BUTTONS_ENABLE


#ifdef CONFIG_BOARD_LEDS_ENABLE
	lowLevelLedInitialization();
#endif	// def CONFIG_BOARD_LEDS_ENABLE


//#ifdef CONFIG_BOARD_USART_ENABLE
	lowLevelUsartInitialization();
//#endif  // def CONFIG_BOARD_USART_ENABLE



}

}	// namespace board

}	// namespace distortos
