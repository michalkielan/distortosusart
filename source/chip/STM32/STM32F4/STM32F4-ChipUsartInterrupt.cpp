#include "include/distortos/chip/ChipUsartInterrupt.hpp"


namespace distortos
{

namespace chip
{

namespace usart
{


void ChipUsartInterrupt::write(const char byte)
{
	while(!isTransmitAvailable());
	waitWriteDataRegisterEmpty();
	writeDataRegister(byte);
	waitTransmissionComplete();
}


std::size_t ChipUsartInterrupt::write(const char* buf, std::size_t nbytes)
{
	while(!isTransmitAvailable());

	tx.buf = const_cast<char *>(buf);
	tx.head = nbytes;

	__DSB();

	writeDataRegister(*tx.buf);
	setTransmitInterruptEnable();

	return nbytes; // TODO: add failer status (timeouts)
}


uint8_t ChipUsartInterrupt::read()
{
	while(!isDataAvailable);
	isDataAvailable = false;
	return tmpReadData;
}


std::size_t ChipUsartInterrupt::read(char* buf, std::size_t count)
{
	for(decltype(count) i{}; i < count; i++)
	{
		*(buf+i) = read();
	}

	return count; // TODO: add failer status (timeouts)
}


void ChipUsartInterrupt::IRQHandler(void)
{
	/**
	 * \Receive Data Interrupt
	 */
	if (getReadDataRegisterNotEmptyStatus())
	{
		clearReadDataRegisterNotEmptyStatus();
		tmpReadData = readDataRegister();
		isDataAvailable = true;
	}


	/**
	 * \Transmit Data Interrupt
	 */
	if (getTransmitDataRegisterEmptyStatus())
	{
		clearTransmitDataRegisterEmptyStatus();

		if (tx.head != 0)
		{
			writeDataRegister(*tx.buf);
			tx.head--;
			tx.buf++;
		}

		else
		{
			clearTransmitInterruptEnable();
		}
	}
}

}	// usart

}	// namespace chip

}	// namespace distortos
