#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTINTERRUPT_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTINTERRUPT_HPP_

#include "distortos/chip/ChipUsart.hpp"

#include <string.h>


namespace distortos
{


namespace chip
{


namespace usart
{


/**
 * \Transmitter/Receiver buffer data
 */
typedef struct
{
	char *buf { nullptr };
	unsigned short head {};
}  BufferData;


/**
 * \ChipInterrupt class
 */
class ChipUsartInterrupt : public ChipUsart
{
	/**
	 * \Receiver buffer data
	 */
	volatile uint8_t tmpReadData {};
	volatile bool isDataAvailable { false };


	/**
	 * \Transmitter buffer data
	 */
	 volatile BufferData tx;


	 /**
	 * \Check if usart is free to Transmit Data
	 */
	bool isTransmitAvailable(void)
	{
		if(tx.head)
			return false;
		else
			return true;
	}


public:

	/**
	 * \ChipUsartInterrupt constructor
	 */
	explicit ChipUsartInterrupt(const Pin tx_, const Pin rx_,
	const PinAlternateFunction txAf_, const PinAlternateFunction rxAf_,
	USART_TypeDef* usart_,
	const IRQn_Type IRQn,
	const PeriphClockSpeed periphClockSpeed_,

	const uint32_t baudrate = 9600,
	const WordLength wordLenght = oneStartBit_8DataBits,
	const Parity parity = none,
	const StopBits stopBits = oneStopBit,
	const TxEnable txEnableStatus = txEnable,
	const RxEnable rxEnableStatus = rxEnable,
	const TxInterruptEnable txInterruptEnableStatus = txInterruptDisable,
	const RxInterruptEnable rxInterruptEnableStatus = rxInterruptDisable
	) : ChipUsart{tx_, rx_, txAf_, rxAf_, usart_, periphClockSpeed_}
	{
		const bool openDrain  = false;
		const PinOutputSpeed outputSpeed = PinOutputSpeed::low;
		const PinPull pull = PinPull::up;

		configureAlternateFunctionPin(tx_, openDrain, outputSpeed, pull, txAf_);
		configureAlternateFunctionPin(rx_, openDrain, outputSpeed, pull, rxAf_);

		configureSetBaudrate(baudrate);
		configureTx(txEnableStatus);
		configureRx(rxEnableStatus);
		configureSetWordLength(wordLenght);
		configureSetParity(parity);
		configureSetStopBits(stopBits);
		configureTxInterrupt(txInterruptEnableStatus);
		configureRxInterrupt(rxInterruptEnableStatus);
		configureUsart(UsartEnable::usartEnable);

		NVIC_EnableIRQ(IRQn);
	}


	/**
	 * \write byte to usart
	 */
	void write(const char byte) override;


	/**
	 * \write buf to usart (POSIX standard)
	 */
	std::size_t write(const char* buf, std::size_t nbytes) override;


	/**
	 * \read byte from usart
	 */
	uint8_t read() override;


	/**
	 * \read buf from usart (POSIX standard)
	 */
	std::size_t read(char* buf, std::size_t count) override;


	/**
	 * \Interrupt Handler
	 */
	void IRQHandler(void) __attribute__ ((flatten));



	/**
	 * \Destructor
	 */
	~ChipUsartInterrupt()
	{

	}


	ChipUsartInterrupt(const ChipUsartInterrupt&) = delete;
	ChipUsartInterrupt(ChipUsartInterrupt&&) = default;
	const ChipUsartInterrupt& operator=(const ChipUsartInterrupt&) = delete;
	ChipUsartInterrupt& operator=(ChipUsartInterrupt&&) = delete;
};



}	// namespace usart

}	// namespace chip

}	// namespace distortos




#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTINTERRUPT_HPP_
