#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTDMA_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTDMA_HPP_

#include "distortos/chip/ChipUsart.hpp"
#include "distortos/chip/STM32F4-DMA.hpp"
#include "distortos/chip/STM32F4-USART.hpp"
#include <string.h>

#include "distortos/chip/clocks.hpp"////clock test

namespace distortos
{


namespace chip
{


namespace usart
{



/**
 * \ChipInterrupt class
 */
class ChipUsartDMA : public ChipUsart
{
	/// transmit busy status TODO propably would be used global
	enum : bool
	{
		TRANSMIT_DATA_DONE,
		TRANSMIT_DATA_IN_PROGRESS
	};

	volatile bool transmitDone {TRANSMIT_DATA_DONE};
	dma::DmaStream dma_stream;

public:

	/**
	 * \ChipUsartInterrupt constructor
	 */
	explicit ChipUsartDMA(const Pin tx_, const Pin rx_,
	const PinAlternateFunction txAf_, const PinAlternateFunction rxAf_,
	USART_TypeDef *usart_,
	dma::DmaStream dma_stream__,
	const IRQn_Type IRQn_dma,
	const PeriphClockSpeed periphClockSpeed_,

	const uint32_t baudrate = 9600,
	const WordLength wordLenght = oneStartBit_8DataBits,
	const Parity parity = none,
	const StopBits stopBits = oneStopBit,
	const TxEnable txEnableStatus = txEnable,
	const RxEnable rxEnableStatus = rxEnable,
	const TxInterruptEnable txInterruptEnableStatus = txInterruptDisable,
	const RxInterruptEnable rxInterruptEnableStatus = rxInterruptDisable
	) : ChipUsart{tx_, rx_, txAf_, rxAf_, usart_, periphClockSpeed_}, dma_stream{dma_stream__}
	{
		const bool openDrain  = false;
		const PinOutputSpeed outputSpeed = PinOutputSpeed::low;
		const PinPull pull = PinPull::up;

		/* usart 6 dma */		//TODO add dma rcc enable in low level initalization
		RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
		RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;


		configureAlternateFunctionPin(tx_, openDrain, outputSpeed, pull, txAf_);
		configureAlternateFunctionPin(rx_, openDrain, outputSpeed, pull, rxAf_);

		configureSetBaudrate(baudrate);
		configureTx(txEnableStatus);
		configureRx(rxEnableStatus);
		configureSetWordLength(wordLenght);
		configureSetParity(parity);
		configureSetStopBits(stopBits);
		configureTxInterrupt(txInterruptEnableStatus);
		configureRxInterrupt(rxInterruptEnableStatus);
		configureDMAtransmiter(txDmaEnable);
		configureDMAreceiver(rxDmaEnable);


		chip::configureUsart(Usart::usart6, chip::RxEnable::rxEnable, chip::TxEnable::txEnable,
				84000000, 9600,
				chip::WordLength::oneStartBit_8DataBits, chip::Parity::none, chip::StopBits::oneStopBit,
				chip::TxInterruptEnable::txInterruptDisable, chip::RxInterruptEnable::rxInterruptDisable,
				chip::DmaTxEnable::txDmaEnable, chip::DmaRxEnable::rxDmaEnable);


		configureDma(
				dma_stream,
				dma::ChannelSelect::usart6_channel,
				dma::PriorityLevel::low,
				dma::TransferInterruptEnable::interrupt_enable,
				dma::DataTransferDirection::memory_to_periph,
				dma::IncrementedMode::incremented);


		configureUsart(UsartEnable::usartEnable);
	    NVIC_EnableIRQ(IRQn_dma);

	}


/**
 * \Write, read virtual methods
 */

	/**
	 * \write byte to usart
	 */
	void write(const char byte) override;


	/**
	 * \write buf to usart (POSIX standard)
	 */
	std::size_t write(const char* buf, std::size_t nbytes) override;


	/**
	 * \read byte from usart
	 */
	uint8_t read() override;


	/**
	 * \read buf from usart (POSIX standard)
	 */
	std::size_t read(char* buf, std::size_t count) override;


/**
 * \Configure usart (DMA) methods
 */


	/**
	 * \ Dma interrupt handler
	 */
	void DMA_IRQHandler() __attribute__ ((flatten));


	~ChipUsartDMA()
	{

	}


	ChipUsartDMA(const ChipUsartDMA&) = delete;
	ChipUsartDMA(ChipUsartDMA&&) = default;
	const ChipUsartDMA& operator=(const ChipUsartDMA&) = delete;
	ChipUsartDMA& operator=(ChipUsartDMA&&) = delete;
};



}	// namespace usart

}	// namespace chip

}	// namespace distortos






#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTDMA_HPP_
