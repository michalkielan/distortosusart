/**
 * \file
 * \brief Header for GPIO-related functions for STM32F4
 *
 * This file covers devices as described in following places:
 * - RM0368 reference manual (STM32F401xB/C and STM32F401xD/E), Revision 4, 2015-05-04
 * - RM0090 reference manual (STM32F405/415, STM32F407/417, STM32F427/437 and STM32F429/439), Revision 11, 2015-10-20
 * - RM0401 reference manual (STM32F410), Revision 2, 2015-10-26
 * - RM0383 reference manual (STM32F411xC/E), Revision 1, 2014-07-24
 * - RM0390 reference manual (STM32F446xx), Revision 1, 2015-03-17
 * - RM0386 reference manual (STM32F469xx and STM32F479xx), Revision 2, 2015-11-19
 *
 * \author Copyright (C) 2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_HPP_

#include "distortos/chip/CMSIS-proxy.h"


#include <utility>

namespace distortos
{

namespace chip
{

namespace dma
{

/*---------------------------------------------------------------------------------------------------------------------+
| global types
+---------------------------------------------------------------------------------------------------------------------*/

// TODO
enum DmaType : uint32_t
{
	dma1 = reinterpret_cast<uint32_t>(DMA1),
	dma2 = reinterpret_cast<uint32_t>(DMA2)
};


enum  DmaStream : uint32_t
{

/*
 * \ DMA1
 */
#ifdef DMA1_Stream0
	/// DMA1 stream0
	dma1_stream0 = reinterpret_cast<uint32_t>(DMA1_Stream0),
#endif // def DMA1_Stream0

#ifdef DMA1_Stream1
	/// DMA1 stream1
	dma1_stream1 = reinterpret_cast<uint32_t>(DMA1_Stream1),
#endif // def DMA1_Stream1

#ifdef DMA1_Stream2
	/// DMA1 stream2
	dma1_stream2 = reinterpret_cast<uint32_t>(DMA1_Stream2),
#endif // def DMA1_Stream2

#ifdef DMA1_Stream3
	/// DMA1 stream3
	dma1_stream3 = reinterpret_cast<uint32_t>(DMA1_Stream3),
#endif // def DMA1_Stream3

#ifdef DMA1_Stream4
	/// DMA1 stream4
	dma1_stream4 = reinterpret_cast<uint32_t>(DMA1_Stream4),
#endif // def DMA1_Stream4

#ifdef DMA1_Stream5
	/// DMA1 stream5
	dma1_stream5 = reinterpret_cast<uint32_t>(DMA1_Stream5),
#endif // def DMA1_Stream5

#ifdef DMA1_Stream6
	/// DMA1 stream6
	dma1_stream6 = reinterpret_cast<uint32_t>(DMA1_Stream6),
#endif // def DMA1_Stream6

#ifdef DMA1_Stream7
	/// DMA1 stream7
	dma1_stream7 = reinterpret_cast<uint32_t>(DMA1_Stream7),
#endif // def DMA1_Stream7


/*
 * \ DMA2
 */
#ifdef DMA2_Stream0
	/// DMA2 stream0
	dma2_stream0 = reinterpret_cast<uint32_t>(DMA2_Stream0),
#endif  // def DMA2_Stream0

#ifdef DMA2_Stream1
	/// DMA2 stream1
	dma2_stream1 = reinterpret_cast<uint32_t>(DMA2_Stream1),
#endif // def DMA2_Stream1

#ifdef DMA2_Stream2
	/// DMA2 stream2
	dma2_stream2 = reinterpret_cast<uint32_t>(DMA2_Stream2),
#endif // def DMA2_Stream2

#ifdef DMA2_Stream3
	/// DMA2 stream3
	dma2_stream3 = reinterpret_cast<uint32_t>(DMA2_Stream3),
#endif // def DMA2_Stream3

#ifdef DMA2_Stream4
	/// DMA2 stream4
	dma2_stream4 = reinterpret_cast<uint32_t>(DMA2_Stream4),
#endif // def DMA2_Stream4

#ifdef DMA2_Stream5
	/// DMA2 stream5
	dma2_stream5 = reinterpret_cast<uint32_t>(DMA2_Stream5),
#endif // def DMA2_Stream5

#ifdef DMA2_Stream6
	/// DMA2 stream6
	dma2_stream6 = reinterpret_cast<uint32_t>(DMA2_Stream6),
#endif // def DMA2_Stream6

#ifdef DMA2_Stream7
	/// DMA2 stream7
	dma2_stream7 = reinterpret_cast<uint32_t>(DMA2_Stream7),
#endif // def DMA2_Stream7

	// USART1 TX DMA Stream7
	usart1_tx_stream7 = dma2_stream7,

	// USART6 TX DMA Stream6
	usart6_tx_stream6 = dma2_stream6,

	// USART6 TX DMA Stream7
	usart6_tx_stream7 = dma2_stream7,

	// USART1 RX DMA Stream5
	usart1_rx_stream5 = dma2_stream5,

	// USART6 RX DMA Stream1
	usart6_rx_stream1 = dma2_stream1,

	// USART6 RX DMA Stream2
	usart6_rx_stream2 = dma2_stream2
};


/// all possible channels for dma
enum ChannelSelect : uint8_t
{
	channel_0_selected,
	channel_1_selected,
	channel_2_selected,
	channel_3_selected,
	channel_4_selected,
	channel_5_selected,
	channel_6_selected,
	channel_7_selected,

	usart1_channel = channel_4_selected,
	usart6_channel = channel_5_selected
};


/// dma enable status
enum DmaEnable : bool
{
	dma_disable,
	dma_enable
};

/// transfer interrupt enable status
enum TransferInterruptEnable : bool
{
	interrupt_disable,
	interrupt_enable
};


/// all possible choises of directions
enum DataTransferDirection : uint8_t
{
	periph_to_memory,
	memory_to_periph,
	memory_to_memory
};

/// all possible choises of priority level
enum PriorityLevel : uint8_t
{
	low,
	medium,
	high,
	veryHigh
};

/// incremented mode
enum IncrementedMode : uint8_t
{
	fixed,
	incremented
};


/*---------------------------------------------------------------------------------------------------------------------+
| global functions
+---------------------------------------------------------------------------------------------------------------------*/



/**
 * \ Set the dma channel
 */
void configureChannel(DMA_Stream_TypeDef& dma_stream, ChannelSelect channel);



/**
 * \ Run DMA
 */
void configureEnable(DMA_Stream_TypeDef& dma_stream, DmaEnable status);;


/**
 * \ Set transmit interrupt complete interrupt enable
 */
void configureTransmitCompleteInterruptEnable(DMA_Stream_TypeDef& dma_stream, TransferInterruptEnable status);


/**
 * \ Set transfer direction
 */
void configureDataTransferDir(DMA_Stream_TypeDef& dma_stream, DataTransferDirection direction);


/**
 * \ configure memory incremented, by default (0) addres pointer is fixed
 */
void configureMemoryIncrementMode(DMA_Stream_TypeDef& dma_stream, IncrementedMode mode);


/**
 * \ configure periph incremented mode, by default (fixed) addres pointer is fixed
 * \ incremented - address are incremented after each data transfered
 */
void configurePeriphIncrementMode(DMA_Stream_TypeDef& dma_stream, IncrementedMode mode);


/**
 * \ configure periph incremented mode, by default (fixed) addres pointer is fixed
 * \ incremented - address are incremented after each data transfered
 */


/**
 * \Return DMA stream TODO : return DMA2 or DMA1 also --> doing in decodeDmaTest()
 */
constexpr DMA_Stream_TypeDef* decodeDmaStream(DmaStream dma_stream);


/**
 * \Return DMA stream and DMA type
 */
constexpr std::pair<DMA_Stream_TypeDef*, DMA_TypeDef*> decodeDma(const DmaStream dma)
{
	////TODO RETURN ONLY DMA2 ! ! !
	return std::make_pair(
			reinterpret_cast<DMA_Stream_TypeDef*>(static_cast<uint32_t>(dma)),
			reinterpret_cast<DMA_TypeDef*>(static_cast<uint32_t>(dma2)) );
}


void configureDma(const DmaStream dmaStream, const ChannelSelect channel, const PriorityLevel priority,
		const TransferInterruptEnable transferInterruptEnableStatus, const DataTransferDirection direction,
		const IncrementedMode memoryIncrementedMode);


}	// namespace dma

}	// namespace chip

}	// namespace distortos

#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_HPP_
