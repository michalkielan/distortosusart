#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTPOLLING_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTPOLLING_HPP_

#include "distortos/chip/ChipUsart.hpp"

namespace distortos
{

namespace chip
{


namespace usart
{

/**
 * \ChipUsartPolling class
 */
class ChipUsartPolling : public ChipUsart
{

public:

	/**
	 * \ChipUsartPolling constructor
	 */
	explicit ChipUsartPolling(const Pin tx_, const Pin rx_,
	const PinAlternateFunction txAf_, const PinAlternateFunction rxAf_,
	USART_TypeDef *usart_,
	const PeriphClockSpeed periphClockSpeed_,

	const uint32_t baudrate = 9600,
	const WordLength wordLenght = oneStartBit_8DataBits,
	const Parity parity = none,
	const StopBits stopBits = oneStopBit,
	const TxEnable txEnableStatus = txEnable,
	const RxEnable rxEnableStatus = rxEnable,
	const TxInterruptEnable txInterruptEnableStatus = txInterruptDisable,
	const RxInterruptEnable rxInterruptEnableStatus = rxInterruptDisable
	) : ChipUsart{tx_, rx_, txAf_, rxAf_, usart_, periphClockSpeed_}
	{
		const bool openDrain  = false;
		const PinOutputSpeed outputSpeed = PinOutputSpeed::low;
		const PinPull pull = PinPull::up;

		configureAlternateFunctionPin(tx_, openDrain, outputSpeed, pull, txAf_);
		configureAlternateFunctionPin(rx_, openDrain, outputSpeed, pull, rxAf_);

		configureSetBaudrate(baudrate);
		configureTx(txEnableStatus);
		configureRx(rxEnableStatus);
		configureSetWordLength(wordLenght);
		configureSetParity(parity);
		configureSetStopBits(stopBits);
		configureTxInterrupt(txInterruptEnableStatus);
		configureRxInterrupt(rxInterruptEnableStatus);
		configureUsart(UsartEnable::usartEnable);
	}


	/**
	 * \write byte to usart (synchronous)
	 */
	void write(const char byte) override
	{
		waitWriteDataRegisterEmpty();
		writeDataRegister(byte);
		waitTransmissionComplete();
	}


	/**
	 * \read byte from usart (synchronous)
	 */
	uint8_t read() override
	{
		waitReadDataRegisterNotEmpty();
		return readDataRegister();
	}


	/**
	 * \write buf to usart (POSIX standard)
	 */
	std::size_t write(const char* buf, std::size_t nbytes) override
	{
		for(decltype(nbytes) i{}; i < nbytes; i++)
		{
			write(char(buf[i]));
		}

		return 1; // TODO: add failer status (timeouts)
	}


	/**
	 * \read buf from usart (POSIX standard)
	 */
	std::size_t read(char* buf, std::size_t count) override
	{
		for(decltype(count) i{}; i < count; i++)
		{
			buf[i] = read();
		}

		return 1; // TODO: add failer status (timeouts)
	}


	~ChipUsartPolling()
	{

	}


	ChipUsartPolling(const ChipUsartPolling&) = delete;
	ChipUsartPolling(ChipUsartPolling&&) = default;
	const ChipUsartPolling& operator=(const ChipUsartPolling&) = delete;
	ChipUsartPolling& operator=(ChipUsartPolling&&) = delete;
};



}	// namespace usart

}	// namespace chip

}	// namespace distortos



#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSARTPOLLING_HPP_
