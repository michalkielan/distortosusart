#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSART_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPUSART_HPP_

#include "distortos/chip/STM32F4-USART-bits.h"
#include "distortos/chip/STM32F4-USART.hpp"
#include "distortos/chip/ChipOutputPin.hpp"


namespace distortos
{

namespace chip
{


enum PeriphClockSpeed : uint32_t
{
	APB1 = 42000000,
	APB2 = 84000000
};


namespace usart
{




/**
 * \ChipUsart
 * \Interface class form ChipUsartPolling, ChipUsartInterrupt, ChipUsartDMA classes
 */
class ChipUsart
{

protected:

	Pin tx;
	Pin rx;
	PinAlternateFunction txAf;
	PinAlternateFunction rxAf;
	USART_TypeDef& usart;
	PeriphClockSpeed periphClockSpeed;


	inline void deconfigureUsart()
	{
		usart.CR1 = 0;
		usart.CR2 = 0;
		usart.CR3 = 0;
		usart.SR = 0;
		usart.BRR = 0;
		usart.DR = 0;
		usart.GTPR = 0;
	}


public:

	ChipUsart(const Pin tx_, const Pin rx_,
		const PinAlternateFunction txAf_, const PinAlternateFunction rxAf_,
		USART_TypeDef* usart_,
		PeriphClockSpeed periphClockSpeed_) :
		tx{tx_},
		rx{rx_},
		txAf{txAf_},
		rxAf{rxAf_},
		usart{*usart_},
		periphClockSpeed{periphClockSpeed_}
		{
		}

/**
 * \Write, read virtual methods
 */
		/**
		 * \write byte to usart
		 */
		virtual void write(const char byte) = 0;


		/**
		 * \read byte from usart
		 */
		virtual uint8_t read() = 0;


		/**
		 * \write buf to usart (POSIX standard)
		 */
		virtual std::size_t write(const char* buf, std::size_t nbytes) = 0;


		/**
		 * \read buf from usart (POSIX standard)
		 */
		virtual std::size_t read(char* buf, std::size_t count) = 0;


		/**
		 * \ TODO add busy status, will be usefull in SerialPort class
		 */
		//virtual bool busyStatus() = 0;



/**
 * \Configure usart methods
 */

		/**
		 * \Enable, disable transmiter
		 */
		void configureTx(TxEnable status);


		/**
		 * \Enable, disable receiver
		 */
		void configureRx(RxEnable status);


		/**
		 * \Set parity (PCE and PS bits)
		 */
		void configureSetParity(Parity parity);


		/**
		 * \Set word lenght ( M bit )
		 */
		void configureSetWordLength(WordLength wordLength);


		/**
		 * \Set baudrate, (using periphClock to count the register value)
		 */
		void configureSetBaudrate(const uint32_t baudrate);


		/**
		 * \Set stop bits (bits 13:12)
		 */
		void configureSetStopBits(StopBits stopbit);


		/**
		 * \Transmission complete interrupt enable/disable
		 */
		void configureTxInterrupt(TxInterruptEnable status);


		/**
		 * \RXNEinterrupt enable/disable
		 */
		void configureRxInterrupt(RxInterruptEnable status);


		/**
		 * \Usart enable/disable
		 */
		void configureUsart(UsartEnable status);


		/**
		 * \Usart DMA transmiter enable/disable
		 */
		void configureDMAtransmiter(DmaTxEnable status);


		/**
		 * \Usart DMA receiver enable/disable
		 */
		void configureDMAreceiver(DmaRxEnable status);


/**
 * \Registers write, read, check status operations
 */

		/**
		 * \ Data Register
		 */
		void writeDataRegister(const char byte);
		uint8_t readDataRegister();


		/**
		 * \Transmit interrupt enable register
		 */
		void setTransmitInterruptEnable();
		void clearTransmitInterruptEnable();


		/**
		 * \Receive interrupt enable register
		 */
		void waitReadDataRegisterNotEmpty();
		bool getReadDataRegisterNotEmptyStatus();
		void clearReadDataRegisterNotEmptyStatus();


		/**
		 * \Transmit data empty register
		 */
		void waitWriteDataRegisterEmpty();
		bool getTransmitDataRegisterEmptyStatus();
		void clearTransmitDataRegisterEmptyStatus();


		/**
		 * \Transmit complete register
		 */
		void waitTransmissionComplete();


		/**
		 * \ Status Register
		 */
		uint32_t readStatusRegister();
		void writeStatusRegister(uint32_t data);


		/**
		 * \Destructor
		 */
		virtual ~ChipUsart()
		{
			deconfigureUsart();
		}

};



}	// namespace usart

}	// namespace chip

}	// namespace distortos




#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_CHIPINPUTPIN_HPP_
