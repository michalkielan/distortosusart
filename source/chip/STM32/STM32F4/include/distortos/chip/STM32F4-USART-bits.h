/**
 * \file
 * \brief Header with definition of bits in USART control registers
 *
 */

#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_BITS_H_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_BITS_H_


#define USART_SR_PE_bit					0
#define USART_SR_FE_bit					1
#define USART_SR_NF_bit					2
#define USART_SR_ORE_bit				3
#define USART_SR_IDLE_bit				4
#define USART_SR_RXNE_bit				5
#define USART_SR_TC_bit					6
#define USART_SR_TXE_bit				7
#define USART_SR_LBD_bit				8
#define USART_SR_CTS_bit				9

#define USART_CR1_SBK_bit				0
#define USART_CR1_RWU_bit				1
#define USART_CR1_RE_bit				2
#define USART_CR1_TE_bit				3
#define USART_CR1_IDLEIE_bit			4
#define USART_CR1_RXNEIE_bit			5
#define USART_CR1_TCIE_bit				6
#define USART_CR1_TXEIE_bit				7
#define USART_CR1_PEIE_bit				8
#define USART_CR1_PS_bit				9
#define USART_CR1_PCE_bit				10
#define USART_CR1_WAKE_bit				11
#define USART_CR1_M_bit					12
#define USART_CR1_UE_bit				13
#define USART_CR1_OVER8_bit				15

#define USART_CR2_ADD0_bit				0
#define USART_CR2_ADD2_bit				1
#define USART_CR2_ADD3_bit				2
#define USART_CR2_LBDL_bit				4
#define USART_CR2_LBDIE_bit				5
#define USART_CR2_LBCL_bit				6
#define USART_CR2_CPHA_bit				7
#define USART_CR2_CPOL_bit				8
#define USART_CR2_CLKEN_bit				9
#define USART_CR2_STOP0_bit				10
#define USART_CR2_STOP1_bit				11
#define USART_CR2_LINEN_bit				12

#define USART_CR3_EIE_bit				0
#define USART_CR3_IREN_bit				1
#define USART_CR3_IRLP_bit				2
#define USART_CR3_HDSEL_bit				3
#define USART_CR3_NACK_bit				4
#define USART_CR3_SCEN_bit				5
#define USART_CR3_DMAR_bit				6
#define USART_CR3_DMAT_bit				7
#define USART_CR3_RTSE_bit				8
#define USART_CR3_CTSE_bit				9
#define USART_CR3_CTSIE_bit				10
#define USART_CR3_ONEBIT_bit			11


#endif /* SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_BITS_H_ */
