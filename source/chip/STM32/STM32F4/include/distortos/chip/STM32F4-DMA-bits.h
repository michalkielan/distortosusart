
#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_BITS_H_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_BITS_H_


#define DMA_SxCR_EN_bit							0
#define DMA_SxCR_DMEIE_bit						1
#define DMA_SxCR_TEIE_bit						2
#define DMA_SxCR_HTIE_bit						3
#define DMA_SxCR_TCIE_bit						4
#define DMA_SxCR_PFCTRL_bit						5
#define DMA_SxCR_DIR_0_bit						6
#define DMA_SxCR_DIR_1_bit						7
#define DMA_SxCR_CIRC_bit						8
#define DMA_SxCR_PINC_bit						9
#define DMA_SxCR_MINC_bit						10
#define DMA_SxCR_PSIZE_0_bit					11
#define DMA_SxCR_PSIZE_1_bit					12
#define DMA_SxCR_MSIZE_0_bit					13
#define DMA_SxCR_MSIZE_1_bit					14
#define DMA_SxCR_PINCOS_bit						15
#define DMA_SxCR_PL_0_bit						16
#define DMA_SxCR_PL_1_bit						17
#define DMA_SxCR_DBM_bit						18
#define DMA_SxCR_CT_bit							19
#define DMA_SxCR_res_bit						20
#define DMA_SxCR_PBURST_0_bit					21
#define DMA_SxCR_PBURST_1_bit					22
#define DMA_SxCR_MBURST_0_bit					23
#define DMA_SxCR_MBURST_1_bit					24
#define DMA_SxCR_CHSEL_0_bit					25
#define DMA_SxCR_CHSEL_1_bit					26
#define DMA_SxCR_CHSEL_2_bit					27


#endif /* SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_DMA_BITS_H_ */







