/**
 * \file
 * \brief Header for GPIO-related functions for STM32F4
 *
 * This file covers devices as described in following places:
 * - RM0368 reference manual (STM32F401xB/C and STM32F401xD/E), Revision 4, 2015-05-04
 * - RM0090 reference manual (STM32F405/415, STM32F407/417, STM32F427/437 and STM32F429/439), Revision 11, 2015-10-20
 * - RM0401 reference manual (STM32F410), Revision 2, 2015-10-26
 * - RM0383 reference manual (STM32F411xC/E), Revision 1, 2014-07-24
 * - RM0390 reference manual (STM32F446xx), Revision 1, 2015-03-17
 * - RM0386 reference manual (STM32F469xx and STM32F479xx), Revision 2, 2015-11-19
 *
 * \author Copyright (C) 2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_HPP_
#define SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_HPP_

#include "distortos/chip/CMSIS-proxy.h"

#include <utility>

namespace distortos
{

namespace chip
{

/*---------------------------------------------------------------------------------------------------------------------+
| global types
+---------------------------------------------------------------------------------------------------------------------*/


/// status of trasmiter
enum TxEnable : uint8_t
{
	txDisable,
	txEnable
};

/// status of receiver
enum RxEnable : uint8_t
{
	rxDisable,
	rxEnable
};

/// all possible number of stop bits
enum StopBits : uint8_t
{
	oneStopBit        = 0x00,
	halfStopBit       = 0x01,
	twoStopBits       = 0x02,
	oneAndHalfStopBit = 0x03
};

///  all possible choises of parity
enum Parity : uint8_t
{
	none = 0b00,
	even = 0b10,
	odd =  0b11
};

/// word length
enum WordLength : uint8_t
{
	oneStartBit_8DataBits,
	oneStartBit_9DataBits
};

/// receive interrupt enable status
enum TxInterruptEnable : uint8_t
{
	txInterruptDisable,
	txInterruptEnable
};

/// receive interrupt enable status
enum RxInterruptEnable : uint8_t
{
	rxInterruptDisable,
	rxInterruptEnable
};

/// usart enable status
enum UsartEnable : uint8_t
{
	usartDisable,
	usartEnable
};

/// dma transmiter enable status
enum DmaTxEnable : uint8_t
{
	txDmaDisable,
	txDmaEnable
};

/// dma receiver enable status
enum DmaRxEnable : uint8_t
{
	rxDmaDisable,
	rxDmaEnable
};

/// identifier of usart
enum  Usart : uint32_t
{

/**
 * \ Usart
 */
#ifdef USART1
	/// USART1
	usart1 = reinterpret_cast<uint32_t>(USART1),
#endif

#ifdef USART2
	/// USART2
	usart2 = reinterpret_cast<uint32_t>(USART2),
#endif

#ifdef USART3
	/// USART3
	usart3 = reinterpret_cast<uint32_t>(USART3),
#endif

#ifdef USART4
	/// USART4
	usart4 = reinterpret_cast<uint32_t>(USART4),
#endif

#ifdef USART5
	/// USART5
	usart5 = reinterpret_cast<uint32_t>(USART5),
#endif

#ifdef USART6
	/// USART6
	usart6 = reinterpret_cast<uint32_t>(USART6),
#endif


/**
 * \ Uart
 */
#ifdef UART1
	/// UART1
	uart1 = reinterpret_cast<uint32_t>(UART1),
#endif

#ifdef UART2
	/// UART2
	uart2 = reinterpret_cast<uint32_t>(UART2),
#endif

#ifdef UART3
	/// UART3
	uart3 = reinterpret_cast<uint32_t>(UART3),
#endif

#ifdef UART4
	/// UART4
	uart4 = reinterpret_cast<uint32_t>(UART4),
#endif

#ifdef UART5
	/// UART5
	uart5 = reinterpret_cast<uint32_t>(UART5),
#endif

#ifdef UART6
	/// UART6
	uart6 = reinterpret_cast<uint32_t>(UART6),
#endif


};


/*---------------------------------------------------------------------------------------------------------------------+
| global functions
+---------------------------------------------------------------------------------------------------------------------*/



/**
 * \ Enable, disable transmiter
 */
void configureTx(USART_TypeDef& usart, TxEnable status);


/**
 * \ Enable, disable receiver
 */
void configureRx(USART_TypeDef& usart, RxEnable status);


/**
 * \ Set parity (PCE and PS bits)
 */
void configureSetParity(USART_TypeDef& usart, Parity parity);


/**
 * \ Set word lenght ( M bit )
 */
void configureSetWordLength(USART_TypeDef& usart, WordLength wordLength);


/**
 * \ Set baudrate, (using periphClock to count the register value)
 */
void configureSetBaudrate(USART_TypeDef& usart, const uint32_t periphClock, const uint32_t baudrate);


/**
 * \ Set stop bits (bits 13:12)
 */
void configureSetStopBits(USART_TypeDef& usart, StopBits stopbit);


/**
 * \ Transmission complete interrupt enable/disable
 */
void configureTxInterrupt(USART_TypeDef& usart, TxInterruptEnable status);


/**
 * \ RXNEinterrupt enable/disable
 */
void configureRxInterrupt(USART_TypeDef& usart, RxInterruptEnable status);


/**
 * \ Usart enable/disable
 */
void configureUsart(USART_TypeDef& usart, UsartEnable status);


/**
 * \ Usart DMA transmiter enable/disable
 */
void configureDMAtransmiter(USART_TypeDef& usart, DmaTxEnable status);


/**
 * \ Usart DMA receiver enable/disable
 */
void configureDMAreceiver(USART_TypeDef& usart, DmaRxEnable status);


/**
 * \ Get the usart type;
 */
constexpr USART_TypeDef* decodeUsart(Usart usart)
{
	return reinterpret_cast<USART_TypeDef*>(static_cast<uint32_t>(usart) );
}


/**
 * \ Configure usart
 */
void configureUsart(const Usart usart, const RxEnable rxStatus, const TxEnable txStatus,
		const uint32_t clk, const uint32_t baudrate,
		const WordLength wordLength, const Parity parity, const StopBits stopBits,
		const TxInterruptEnable txInterruptStatus, const RxInterruptEnable rxInterruptStatus,
		const DmaTxEnable dmaTxStatus, const DmaRxEnable dmaRxStatus);

}	// namespace chip

}	// namespace distortos

#endif	// SOURCE_CHIP_STM32_STM32F4_INCLUDE_DISTORTOS_CHIP_STM32F4_USART_HPP_
