#include "include/distortos/chip/ChipUsart.hpp"
#include "distortos/chip/STM32F4-USART.hpp"
#include "distortos/architecture/ARMv7-M-bit-banding.h"



namespace distortos
{

namespace chip
{

namespace usart
{



void ChipUsart::configureTx(TxEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureTx(usartDecode,status);
}


void ChipUsart::configureRx(RxEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureRx(usartDecode,status);
}


void ChipUsart::configureSetParity(Parity parity)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureSetParity(usartDecode,parity);
}


void ChipUsart::configureSetWordLength(WordLength wordLength)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureSetWordLength(usartDecode,wordLength);
}


void ChipUsart::configureSetBaudrate(const uint32_t baudrate)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureSetBaudrate(usartDecode, 84000000, baudrate);
}


void ChipUsart::configureSetStopBits(StopBits stopbit)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureSetStopBits(usartDecode, stopbit);
}


void ChipUsart::configureTxInterrupt(TxInterruptEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureTxInterrupt(usartDecode, status);
}


void ChipUsart::configureRxInterrupt(RxInterruptEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureRxInterrupt(usartDecode, status);
}


void ChipUsart::configureUsart(UsartEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureUsart(usartDecode, status);
}


void ChipUsart::configureDMAtransmiter(DmaTxEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureDMAtransmiter(usartDecode, status);
}


void ChipUsart::configureDMAreceiver(DmaRxEnable status)
{
	auto& usartDecode = *decodeUsart(Usart::usart6);
	chip::configureDMAreceiver(usartDecode, status);
}


void ChipUsart::writeDataRegister(const char byte)
{
	usart.DR = byte;
}


uint8_t ChipUsart::readDataRegister()
{
	return (usart.DR & 0x1FF);
}


void ChipUsart::setTransmitInterruptEnable()
{
	usart.CR1 |= USART_CR1_TXEIE;
}


void ChipUsart::clearTransmitInterruptEnable()
{
	usart.CR1 &= ~USART_CR1_TXEIE;
}


void ChipUsart::waitReadDataRegisterNotEmpty()
{
	while ((usart.SR & USART_SR_RXNE) == 0);
}


bool ChipUsart::getReadDataRegisterNotEmptyStatus()
{
	return (usart.SR & USART_SR_RXNE);
}


void ChipUsart::clearReadDataRegisterNotEmptyStatus()
{
	usart.SR &= ~USART_SR_RXNE;
}


void ChipUsart::waitWriteDataRegisterEmpty()
{
	while ((usart.SR & USART_SR_TXE) == 0);
}


bool ChipUsart::getTransmitDataRegisterEmptyStatus()
{
	return (usart.SR & USART_SR_TXE);
}


void ChipUsart::clearTransmitDataRegisterEmptyStatus()
{
	usart.SR &= ~USART_SR_TXE;
}


void ChipUsart::waitTransmissionComplete()
{
	while ((usart.SR & USART_SR_TC) == 0);
}


uint32_t ChipUsart::readStatusRegister()
{
	return usart.SR;
}


void ChipUsart::writeStatusRegister(uint32_t data)
{
	usart.SR = data;
}


}	// usart

}	// namespace chip

}	// namespace distortos
