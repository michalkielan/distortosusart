

#include "distortos/chip/STM32F4-USART.hpp"
#include "distortos/chip/STM32F4-USART-bits.h"
#include "distortos/architecture/ARMv7-M-bit-banding.h"
#include <utility>

namespace distortos
{

namespace chip
{

/*---------------------------------------------------------------------------------------------------------------------+
| global types
+---------------------------------------------------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------------------------------------------------+
| global functions
+---------------------------------------------------------------------------------------------------------------------*/

void configureTx(USART_TypeDef& usart, TxEnable status)
{
	BITBAND(&usart.CR1, USART_CR1_TE_bit) = status;
}


void configureRx(USART_TypeDef& usart, RxEnable status)
{
	BITBAND(&usart.CR1, USART_CR1_RE_bit) = status;
}

void configureSetParity(USART_TypeDef& usart, Parity parity)
{
	BITBAND(&usart.CR2, USART_CR2_STOP0_bit) =  (1 & parity);
	BITBAND(&usart.CR2, USART_CR2_STOP1_bit) = ((2 & parity) >> 1);
}


void configureSetWordLength(USART_TypeDef& usart, WordLength wordLength)
{
	BITBAND(&usart.CR1, USART_CR1_M_bit) = wordLength;
}


void configureSetBaudrate(USART_TypeDef& usart, const uint32_t periphClock, const uint32_t baudrate)
{
	usart.BRR = periphClock/baudrate;
}


void configureSetStopBits(USART_TypeDef& usart, StopBits stopbit)
{
	BITBAND(&usart.CR2, USART_CR2_STOP0_bit) =  (1 & stopbit);
	BITBAND(&usart.CR2, USART_CR2_STOP1_bit) = ((2 & stopbit) >> 1);
}


void configureTxInterrupt(USART_TypeDef& usart, TxInterruptEnable status)
{
	BITBAND(&usart.CR1, USART_CR1_TXEIE_bit) = status;
}


void configureRxInterrupt(USART_TypeDef& usart, RxInterruptEnable status)
{
	BITBAND(&usart.CR1, USART_CR1_RXNEIE_bit) = status;
}


void configureUsart(USART_TypeDef& usart, UsartEnable status)
{
	BITBAND(&usart.CR1, USART_CR1_UE_bit) = status;
}


void configureDMAtransmiter(USART_TypeDef& usart, DmaTxEnable status)
{
	BITBAND(&usart.CR3, USART_CR3_DMAT_bit) = status;
}


void configureDMAreceiver(USART_TypeDef& usart, DmaRxEnable status)
{
	BITBAND(&usart.CR3, USART_CR3_DMAR_bit) = status;
}


void configureUsart(const Usart usart, const RxEnable rxStatus, const TxEnable txStatus,
		const uint32_t clk, const uint32_t baudrate,
		const WordLength wordLength, const Parity parity, const StopBits stopBits,
		const TxInterruptEnable txInterruptStatus, const RxInterruptEnable rxInterruptStatus,
		const DmaTxEnable dmaTxStatus, const DmaRxEnable dmaRxStatus)
{
	auto& usartPort = *decodeUsart(usart) ;

	configureSetBaudrate(usartPort, clk, baudrate);
	configureTx(usartPort, txStatus);
	configureRx(usartPort, rxStatus);
	configureSetWordLength(usartPort, wordLength);
	configureSetParity(usartPort, parity);
	configureSetStopBits(usartPort, stopBits);
	configureTxInterrupt(usartPort, txInterruptStatus);
	configureRxInterrupt(usartPort, rxInterruptStatus);
	configureDMAtransmiter(usartPort, dmaTxStatus);
	configureDMAreceiver(usartPort, dmaRxStatus);

	configureUsart(usartPort, UsartEnable::usartEnable);
}



}	// namespace chip

}	// namespace distortos

