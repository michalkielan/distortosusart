/**
 * \file
 * \brief Header for GPIO-related functions for STM32F4
 *
 * This file covers devices as described in following places:
 * - RM0368 reference manual (STM32F401xB/C and STM32F401xD/E), Revision 4, 2015-05-04
 * - RM0090 reference manual (STM32F405/415, STM32F407/417, STM32F427/437 and STM32F429/439), Revision 11, 2015-10-20
 * - RM0401 reference manual (STM32F410), Revision 2, 2015-10-26
 * - RM0383 reference manual (STM32F411xC/E), Revision 1, 2014-07-24
 * - RM0390 reference manual (STM32F446xx), Revision 1, 2015-03-17
 * - RM0386 reference manual (STM32F469xx and STM32F479xx), Revision 2, 2015-11-19
 *
 * \author Copyright (C) 2016 Kamil Szczygiel http://www.distortec.com http://www.freddiechopin.info
 *
 * \par License
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "distortos/chip/CMSIS-proxy.h"
#include "distortos/chip/STM32F4-DMA-bits.h"
#include "distortos/architecture/ARMv7-M-bit-banding.h"
#include "distortos/chip/STM32F4-DMA.hpp"

#include <utility>

namespace distortos
{

namespace chip
{

namespace dma
{


/*---------------------------------------------------------------------------------------------------------------------+
| global functions
+---------------------------------------------------------------------------------------------------------------------*/



void configureChannel(DMA_Stream_TypeDef& dma_stream, ChannelSelect channel)
{
	BITBAND(&dma_stream, DMA_SxCR_CHSEL_0_bit) =  (1 & channel);
	BITBAND(&dma_stream, DMA_SxCR_CHSEL_1_bit) = ((2 & channel) >> 1);
	BITBAND(&dma_stream, DMA_SxCR_CHSEL_2_bit) = ((4 & channel) >> 2);
}



void configureEnable(DMA_Stream_TypeDef& dma_stream, DmaEnable status)
{
	BITBAND(&dma_stream, DMA_SxCR_EN) = status;
}


void configureTransmitCompleteInterruptEnable(DMA_Stream_TypeDef& dma_stream, TransferInterruptEnable status)
{
	BITBAND(&dma_stream, DMA_SxCR_TCIE_bit) = status;
}


void configureDataTransferDir(DMA_Stream_TypeDef& dma_stream, DataTransferDirection direction)
{
	BITBAND(&dma_stream, DMA_SxCR_DIR_0_bit) =  (1 & direction);
	BITBAND(&dma_stream, DMA_SxCR_DIR_1_bit) = ((2 & direction) >> 1);
}


void configureMemoryIncrementMode(DMA_Stream_TypeDef& dma_stream, IncrementedMode mode)
{
	BITBAND(&dma_stream, DMA_SxCR_MINC_bit) = mode;
}


void configurePeriphIncrementMode(DMA_Stream_TypeDef& dma_stream, IncrementedMode mode)
{
	BITBAND(&dma_stream, DMA_SxCR_PINC_bit) = mode;
}


void configurePriorityLevel(DMA_Stream_TypeDef& dma_stream, PriorityLevel level)
{
	BITBAND(&dma_stream, DMA_SxCR_PL_0_bit) =  (1 & level);
	BITBAND(&dma_stream, DMA_SxCR_PL_1_bit) = ((2 & level) >> 1);
}


constexpr DMA_Stream_TypeDef* decodeDmaStream(DmaStream dma_stream)
{
	return reinterpret_cast<DMA_Stream_TypeDef*>(static_cast<uint32_t>(dma_stream) );
}


void configureDma(const DmaStream dmaStream, const ChannelSelect channel, const PriorityLevel priority,
		const TransferInterruptEnable transferInterruptEnableStatus, const DataTransferDirection direction,
		const IncrementedMode memoryIncrementedMode)
{
	auto& dma_stream = *decodeDmaStream(dmaStream);

	configureChannel(dma_stream, channel);
	configurePriorityLevel(dma_stream, priority);
	configureTransmitCompleteInterruptEnable(dma_stream, transferInterruptEnableStatus);
	configureDataTransferDir(dma_stream, direction);
	configureMemoryIncrementMode(dma_stream, memoryIncrementedMode);
}




}	// namespace dma

}	// namespace chip

}	// namespace distortos

