#include "distortos/chip/ChipUsartDMA.hpp"
#include "distortos/architecture/ARMv7-M-bit-banding.h"
#include "distortos/chip/STM32F4-DMA-bits.h"

#include "distortos/board/leds.hpp"	// debug leds///////////


namespace distortos
{

namespace chip
{

namespace usart
{


void ChipUsartDMA::write(const char byte)
{
	waitWriteDataRegisterEmpty();
	writeDataRegister(byte);
	waitTransmissionComplete();
}


std::size_t ChipUsartDMA::write(const char* buf, std::size_t nbytes)
{
	auto& dmaStream = *dma::decodeDma(dma_stream).first;

	while(transmitDone != TRANSMIT_DATA_DONE);
	transmitDone = TRANSMIT_DATA_IN_PROGRESS;

	BITBAND(&dmaStream.CR, DMA_SxCR_EN_bit) = 0;
	dmaStream.NDTR = nbytes;
	dmaStream.PAR = reinterpret_cast<uint32_t>(&usart.DR);
	dmaStream.M0AR = reinterpret_cast<uint32_t>(buf);
	BITBAND(&dmaStream.CR, DMA_SxCR_EN_bit) = 1;

	return nbytes; // TODO: add failer status (timeouts)
}


uint8_t ChipUsartDMA::read()
{
	return 0;
}


std::size_t ChipUsartDMA::read(char* buf, std::size_t count)
{
	buf[0] = 'i';
	return count; // TODO: add failer status (timeouts)
}




void ChipUsartDMA::DMA_IRQHandler()
{
	const auto decodeDma = dma::decodeDma(dma_stream);
	auto& DMA = *decodeDma.second;
	auto& dmaStream = *decodeDma.first;

	if (DMA.HISR & DMA_HISR_TCIF6)
	{
		DMA.HIFCR |= DMA_HIFCR_CTCIF6;
		BITBAND(&dmaStream.CR, DMA_SxCR_EN_bit) = 0;
		transmitDone = TRANSMIT_DATA_DONE;
	}
}


}	// usart

}	// namespace chip

}	// namespace distortos
