CMSIS for STM32F0
=================

- link: [STM32CubeF0](http://www.st.com/web/catalog/tools/FM147/CL1794/SC961/SS1743/LN1897/PF260612)
- package name: STM32Cube_FW_F0_V1.5.0
- package version: V1.5.0
- download date: 2016-03-20

Only headers from STM32Cube_FW_F0_V1.5.0/Drivers/CMSIS/Device/ST/STM32F0xx/Include were copied from the package.
