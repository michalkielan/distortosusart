CMSIS for STM32F1
=================

- link: [STM32CubeF1](http://www.st.com/st-web-ui/active/en/catalog/tools/PF260820)
- package name: STM32Cube_FW_F1_V1.3.0
- package version: V1.3.0
- download date: 2016-02-10

Only headers from STM32Cube_FW_F1_V1.3.0/Drivers/CMSIS/Device/ST/STM32F1xx/Include were copied from the package.
