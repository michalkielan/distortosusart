/*
 * abort.hpp
 *
 *  Created on: Apr 21, 2016
 *      Author: michal
 */

#ifndef INCLUDE_DISTORTOS_ABORT_HPP_
#define INCLUDE_DISTORTOS_ABORT_HPP_

#include "distortos/ThisThread.hpp"
#include "distortos/board/leds.hpp"	// debug leds

#include <string.h>


namespace distortos
{


namespace error
{

inline void  __attribute__((noreturn)) abort();

inline void abort()
{
	auto &errorLed = distortos::board::leds[3];
	while(1)
	{
		errorLed.set(!errorLed.get());
		distortos::ThisThread::sleepFor(std::chrono::milliseconds{ 500 });
	}
}


}	// namespace error


}	// namespace distortos




#endif /* INCLUDE_DISTORTOS_ABORT_HPP_ */
