/*
 * serial.h
 *
 *  Created on: Apr 8, 2016
 *      Author: michal
 */

#ifndef INCLUDE_DISTORTOS_SERIALPORT_SERIALPORT_HPP_
#define INCLUDE_DISTORTOS_SERIALPORT_SERIALPORT_HPP_

#include "distortos/Mutex.hpp"
#include "distortos/StaticThread.hpp"
#include "distortos/Semaphore.hpp"
#include "distortos/DynamicThread.hpp"
#include "distortos/chip/ChipUsart.hpp"
#include "distortos/abort.hpp"
#include "distortos/string/string.hpp"

#include <array>
#include <type_traits>
#include <string.h>


namespace distortos
{


/**
 * \SerialPort class
 * \implemented similar to SerialPort in .NET framework
 */
class SerialPort
{
	/**
	 * \Locker to lock the data using in write function
	 */
	mutable Mutex writeLocker;


	/**
	 * \Locker to lock the data in printf function
	 */
	mutable Mutex printLocker;


	/**
	 * \Polymorphic reference to abstract Usart class
	 */
	chip::usart::ChipUsart& usart;


	/**
	 * Check if read async thread is running
	 */
	bool isAsyncThreadRunning { false };


	/**
	 * \Write protected data, using RAII pattern
	 */
	void writeProtected(chip::usart::ChipUsart& u, const char* data, std::size_t len);


	/**
	 * \Type safety print implementation, not thread safe,
	 * \for details looks thread safe implementation -> print(const T& s, const Arg&... arg)
	 */
	void print_()
	{
	}

	template<typename T, typename... Arg>
	void print_(const T& s, const Arg&... arg)
	{
		*this << s;
		print_(arg...);
	}

public:

	/**
	 * \Initializes a new instance of the SerialPort class with default ChipUsart class parameters
	 */
	SerialPort(chip::usart::ChipUsart& usart_);


	/**
	 * \Initializes a new instance of the SerialPort with specified baudrate
	 */
	SerialPort(chip::usart::ChipUsart& usart_,
			const uint32_t baudrate);


	/**
	 * \Initializes a new instance of the SerialPort with specified baudrate and parity
	 */
	SerialPort(chip::usart::ChipUsart& usart_,
			const uint32_t baudrate,
			chip::Parity parity);


	/**
	 * \Initializes a new instance of the SerialPort with specified baudrate, parity, word length
	 */
	SerialPort(chip::usart::ChipUsart& usart_,
			const uint32_t baudrate,
			chip::Parity parity,
			chip::WordLength wordLength);


	/**
	 * \Initializes a new instance of the SerialPort with specified baudrate, parity, word lenght, stop bits
	 */
	SerialPort(chip::usart::ChipUsart& usart_,
			const uint32_t baudrate,
			chip::Parity parity,
			chip::WordLength wordLength,
			chip::StopBits stopBits);


	/**
	 * \Create Aynchronous read/write thread
	 * \thread function must be looks like: void handler(SerialPort& s);
	 * \thread must be fun only once, if not, error function will be called
	 */
	template<typename HandlerFunction>
    void startAsyncRead(const std::size_t stackSize, const uint8_t priority, HandlerFunction&& function)
    {
		if(!isAsyncThreadRunning)
		{
			DynamicThread t = makeAndStartDynamicThread({stackSize, priority}, std::forward<HandlerFunction>(function), std::ref(*this));
			t.detach();
			isAsyncThreadRunning = true;
		}

		else
		{
			writeLn("Serial Port Error: Async Thread is already created");
			error::abort();
		}
    }


	/**
	 * \return the Usart object
	 */
	chip::usart::ChipUsart& getUsartObject();


	/**
	 * \Set number of numbers print after floating point
	 * \similar to std::cout.precision(n)
	 */
	void setPrecision(decltype(floatingPointPrecision) setPrecision);


	/**
	 * \write data (std::string)
	 */
	void write(const std::string& data);


	/**
	 * \write data with endl (std::string)
	 */
	void writeLn(const std::string& data);


	/**
	 * \write array
	 */
	template<std::size_t N>
	void write(const uint8_t (& buf)[N])
	{
		writeProtected(usart, buf,N);
	}


	/**
	 * \write array with knowing size
	 */
	void write(const char* buf, std::size_t N);


	/**
	 * \write array (std::array type)
	 */
	template<typename T, std::size_t N>
	void write(std::array<T, N> buf)
	{
		writeProtected(usart, buf.data(), N);
	}


	/**
	 * \read to character (synchronous)
	 */
	std::string readTo(const char end);


	/**
	 * \read line
	 */
	std::string readLine();


	/**
	 * \print, using like: ("data1: ", 5, " data2: ", 6, "\n");
	 * \not using like C - printf with %d, %f
	 * \thread safe with his own mutex locker
	 */
	template<typename T, typename... Arg>
	void print(const T& s, const Arg&... arg)
	{
		printLocker.lock();
		print_(s, arg...);
		printLocker.unlock();
	}


	/**
	 * \Serial Port overloaded operators <<
	 * \similar to std::cout <<
	 */
	SerialPort& operator<<(const std::string& data);
	SerialPort& operator<<(const char* data);
	SerialPort& operator<<(char byte);

	template<typename NumericType,
	typename = typename std::enable_if<std::is_arithmetic<NumericType>::value, NumericType>::type>
	SerialPort& operator<<(NumericType num)
	{
		static auto str{to_string(num)};
		*this << str;
		return *this;
	}

};


}	// namespace distortos





#endif /* INCLUDE_DISTORTOS_SERIALPORT_SERIALPORT_HPP_ */
