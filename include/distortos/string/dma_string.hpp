/*
 * dma_string.hpp
 *
 *  Created on: Apr 21, 2016
 *      Author: michal
 */

#ifndef INCLUDE_DISTORTOS_STRING_DMA_STRING_HPP_
#define INCLUDE_DISTORTOS_STRING_DMA_STRING_HPP_

#include "distortos/chip/CMSIS-proxy.h"
#include <string>
#include <string.h>


/**
 * \Copy block of memory using DMA
 * \similar as memcpy
 */
void* dma_memcpy(void* _dest, const void* _src, std::size_t _count);


/**
 * \Copy string using DMA
 * \similar as strcpy
 */
char* dma_strcpy(char* _dest, const char* _src);


/**
 * \Copy characters from string using DMA
 * \similar as strncpy
 */
char* dma_strncpy(char* _dest, const char* _src,  std::size_t _num);


/**
 * \Concatenate strings using DMA
 * \similar as strcat
 */
char* dma_strcat(char* _dest, const char* _src);


#endif /* INCLUDE_DISTORTOS_STRING_DMA_STRING_HPP_ */
