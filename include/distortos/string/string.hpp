#ifndef INCLUDE_DISTORTOS_STRING_HPP_
#define INCLUDE_DISTORTOS_STRING_HPP_

#include <string>
#include <math.h>

namespace distortos
{

/*
 * floating point print precise (at the moment global variable
 * TODO: wait for SerialPort Class 100% done and see if is needed as global
 */
extern unsigned int floatingPointPrecision;


/*
 * maximal unsigned value type, used in printring functions
 */
using maxUnsignedType = unsigned long long;


/*
 * end of line characters
 */
enum : char
{
	endl = '\n',
	flush = '\r'
};



/*
 * Convert to string functions, simmilar to std::to_string()
 * goal: not using sprintf or ostream
 */
std::string to_string( int value );
std::string to_string( long value );
std::string to_string( long long value );
std::string to_string( unsigned value );
std::string to_string( unsigned long value );
std::string to_string( unsigned long long value );
std::string to_string( float value );
std::string to_string( double value );
std::string to_string( long double value );


/*
 * unsigned type to string
 */
template<typename UnsignedType,
typename = typename std::enable_if<std::is_unsigned <UnsignedType>::value, UnsignedType>::type>
static std::string unsigned_to_string(UnsignedType value)
{
	std::string tmp {};
	std::string ret {};

	do
	{
		tmp += value % 10 + '0';
		value -= value % 10;
	} while (value /= 10);

	for (int i = tmp.size() - 1; i >= 0; i--)
	{
		ret += tmp[i];
	}

	return ret;
}


/*
 * signed type to string
 */
template<typename IntegralType,
typename = typename std::enable_if<std::is_integral <IntegralType>::value, IntegralType>::type>
static std::string signed_to_string(IntegralType value)
{
	std::string tmp {};
	std::string ret {};

	if (value < 0)
	{
		ret = "-";
		value = -value;
	}

	do
	{
		tmp += value % 10 + '0';
		value -= value % 10;
	} while (value /= 10);

	for (int i= tmp.size() - 1; i >= 0; i--) {
		ret += tmp[i];
	}

	return ret;
}



/*
 * floating point (float, double) type to string
 */
template<typename FloatingPointType,
typename = typename std::enable_if<std::is_floating_point <FloatingPointType>::value, FloatingPointType>::type>
static std::string fp_to_string(FloatingPointType value, unsigned int& precise)
{
	const unsigned int maxPrecise {16};

	std::string tmp {};
	std::string ret {};
	std::string afterFp {};

	maxUnsignedType valueInt {};
	unsigned int numOfDigits {};

	if (value < 0)
	{
		ret = "-";
		value = -value;
	}

	if(value < 1)
	{
		if(precise > maxPrecise)
			precise = maxPrecise;

		value = value * pow(10, precise);

		ret += "0.";
		ret += unsigned_to_string((maxUnsignedType)value);

		return ret;
	}

	valueInt = value;

	do
	{
		tmp += valueInt % 10 + '0';
		valueInt -= valueInt % 10;
		numOfDigits++;
	} while (valueInt /= 10);

	for (int i = tmp.size() - 1; i >= 0; i--)
	{
		ret += tmp[i];
	}

	if(precise + numOfDigits > maxPrecise)
	{
		precise = maxPrecise - numOfDigits;
	}

	value = value * pow(10, precise);

	afterFp = unsigned_to_string( (maxUnsignedType)value );

	afterFp.erase(0, numOfDigits);

	ret += ".";

	if(!afterFp.empty())
	{
		ret += afterFp;
	}

	else
	{
		ret += "00";
	}

	return ret;
}


template<typename NumericType,
typename = typename std::enable_if<std::is_arithmetic<NumericType>::value, NumericType>::type>
std::string hex(NumericType value)
{
	static const char* digits = "0123456789ABCDEF";
	auto hexLen = sizeof(NumericType)<<1;
    std::string rc(hexLen,'0');

    for (decltype(hexLen) i{}, j = (hexLen-1)*4 ; i < hexLen; ++i, j-=4)
    {
        rc[i] = digits[(value>>j) & 0x0F];
    }

    rc = "0x" + rc;

    return rc;
}



}	// namespace distortos



#endif	// INCLUDE_DISTORTOS_STRING_HPP_
